# l2dafee

## ファイル説明

- GAE - ホームページ(http://l2dafee.appspot.com)のソースコード群。
- ざしき.cmo3 - Live2Dモデル(LIve2D Cubism Editor)
- ざしき_素材分け.clip - 素材分け(CLIP STUDIO PAINT)


ホームページはSDKのサンプルコードを使用しています。
http://sites.cybernoids.jp/cubism-sdk2/webgl2-1

ざしきちゃんはAFEE(エンターテイメント表現の自由の会)のキャラクターです。
https://afee.jp/
